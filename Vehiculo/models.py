from django.db import models
from django.utils import timezone

# Create your models here.

class Vehiculo(models.Model):
    marca = models.CharField(max_length=30, blank=True, null=True)
    modelo = models.CharField(max_length=30, blank=True, null=True)
    año = models.CharField(max_length=30, blank=True, null=True)
    color = models.CharField(max_length=20, blank=True, null=True)
    numeroPuertas = models.CharField(max_length=30, blank=True, null=True)
    descripcion = models.CharField(max_length=100, blank=True, null=True)
    fechaPublicacion = models.DateTimeField(auto_now_add=True, auto_now=False)
    precio = models.CharField(max_length=30, blank=True, null=True)
